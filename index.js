const got = require('got');
const htmlparser2 = require('htmlparser2');
const fs = require('fs');
var child_proc = require('child_process');

const recordProducts = async () => {
	fs.rmSync('products.txt');
	const response = await got('https://www.lttstore.com')
	const parser = new htmlparser2.Parser({
		onopentag(name, attribs) {
			if (name === "a" && attribs.class === "ProductItem__ImageWrapper ") {
				fs.appendFileSync('products.txt',
					`https://www.lttstore.com${attribs.href}\n`);

			};
		}
	});
	parser.write(response.body);
	parser.end();
};

const getProductsFromFile = () => {
	const file = fs.readFileSync('products.txt', 'UTF-8');

	return file.split(/\r?\n/);
};

const fetchProducts = async () => {
	const response = await got('https://www.lttstore.com')
	const products = [];
	const parser = new htmlparser2.Parser({
		onopentag(name, attribs) {
			if (name === "a" && attribs.class === "ProductItem__ImageWrapper ") {
				products.push(`https://www.lttstore.com${attribs.href}`);
			};
		}
	});
	parser.write(response.body);
	parser.end();

	return products;
};

const poll = async () => {
    const historical = getProductsFromFile();
    var count = 0; 

	while (true) {
        console.log('checking:', count+=1);        
        const current = await fetchProducts();
        // console.log(current);
        const difference = current.filter(x => !historical.includes(x));
        console.log(difference);
		if (!Array.isArray(difference) || !difference.length) {
            console.log('nothing yet');
            await new Promise(resolve => setTimeout(resolve, 5000));
		} else {
            console.log('diff', difference[0]);
            return difference[0];
		}
	}
};

(async () => {
	try {
		await recordProducts();
        const new_url = await poll();
        child_proc.exec(`open -a "Google Chrome" ${new_url}`);
	} catch (e) {
		console.error('something bad happened', e);
	}
})();
